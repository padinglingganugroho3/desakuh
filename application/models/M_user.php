<?php

class M_user extends CI_Model
{
    private $_user = 'user';

    public function __construct()
    {
        parent::__construct();
        $this->load->database(); // Memuat library database
    }

    public function doLogin($username, $password)
    {
        $query = $this->db->get_where($this->_user, ['username' => $username, 'password' => $password]);

        if ($query->num_rows() == 1) {
            return $query->row();
        } else {
            return false;
        }
    }
}
